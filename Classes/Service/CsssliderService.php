<?php

namespace HIVE\HiveCptCntCssslider\Service;

/***
 *
 * This file is part of the "hive_cpt_cnt_cssslider" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Andreas Hafner <a.hafner@teufels.com>, teufels GmbH
 *           Dominik Hilser <d.hilser@teufels.com>, teufels GmbH
 *           Georg Kathan <g.kathan@teufels.com>, teufels GmbH
 *           Hendrik Krüger <h.krueger@teufels.com>, teufels GmbH
 *           Josymar Escalona Rodriguez <j.rodriguez@teufels.com>, teufels GmbH
 *           Perrin Ennen <p.ennen@teufels.com>, teufels GmbH
 *           Timo Bittner <t.bittner@teufels.com>, teufels GmbH
 *
 ***/

/**
 * Class CsssliderService
 * @package HIVE\HiveCptCntCssslider\Service
 */
class CsssliderService implements \TYPO3\CMS\Core\SingletonInterface
{

    /**
     * @param int $iSlides
     * @param string $sUniqueIdentifier
     * @param array $aClasses
     * @param array $aNavLabelsOverrides
     *
     * @return string
     */
    public function createSliderHtml(
        int $iSlides = 0,
        string $sUniqueIdentifier = "",
        array $aClasses = [1, 1, 1, 1, 1],
        array $aNavLabelsOverrides = []
    ): string {

        if ($aClasses[0] < 1 or $aClasses[0] > 1) {
            return "<div class=\"alert alert-danger\" role=\"alert\">Error " . __CLASS__ . " " . __FUNCTION__ . " [" . __LINE__ . "]</div>";
        }
        if ($aClasses[1] < 1 or $aClasses[1] > 1) {
            return "<div class=\"alert alert-danger\" role=\"alert\">Error " . __CLASS__ . " " . __FUNCTION__ . " [" . __LINE__ . "]</div>";
        }
        if ($aClasses[2] < 1 or $aClasses[2] > 2) {
            return "<div class=\"alert alert-danger\" role=\"alert\">Error " . __CLASS__ . " " . __FUNCTION__ . " [" . __LINE__ . "]</div>";
        }
        if ($aClasses[3] < 1 or $aClasses[3] > 3) {
            return "<div class=\"alert alert-danger\" role=\"alert\">Error " . __CLASS__ . " " . __FUNCTION__ . " [" . __LINE__ . "]</div>";
        }
        if ($aClasses[4] < 1 or $aClasses[4] > 5) {
            return "<div class=\"alert alert-danger\" role=\"alert\">Error " . __CLASS__ . " " . __FUNCTION__ . " [" . __LINE__ . "]</div>";
        }

        $iCount = $iSlides;

        if ($iCount < 1 || $iCount > 8) {
            return "<div class=\"alert alert-danger\" role=\"alert\">Error " . __CLASS__ . " " . __FUNCTION__ . " [" . __LINE__ . "]</div>";
        }

        foreach ($aClasses as $iK => $iV) {
            if ($iCount < $iV) {
                $aClasses[$iK] = $iCount;
            }
        }

        $sInputs = "";
        $sDots = "";

        $sArrowsPrev = "";

        $sArrowsNext = "";
        $sSlides = "";
        for ($i = 1; $i <= $iCount; $i++) {
            $h = $i - 1;
            $j = $i + 1;
            $sInputs .= "<input " . ($i == 1 ? 'checked' : '') . " id='slide__$sUniqueIdentifier--$i' name='s--$sUniqueIdentifier' type='radio' class='csss_input'>";

            $sNavLabel = (array_key_exists($i-1, $aNavLabelsOverrides) && $aNavLabelsOverrides[$i-1] != "" ? $aNavLabelsOverrides[$i-1] : $i);
            $sDots .= "<label class='dot btn btn-secondary' for='slide__$sUniqueIdentifier--$i'>$sNavLabel</label>";

            /*
             * XS and SM
             */
            if ($i == 1) {
                $sArrowsPrev .= "<label class='prev ' for='slide__$sUniqueIdentifier--$iCount'>&laquo;</label>";
            } else {
                $sArrowsPrev .= "<label class='prev' for='slide__$sUniqueIdentifier--$h'>&laquo;</label>";
            }
            if ($i == $iCount) {
                $sArrowsNext .= "<label class='next' for='slide__$sUniqueIdentifier--1'>&raquo;</label>";
            } else {
                $sArrowsNext .= "<label class='next' for='slide__$sUniqueIdentifier--$j'>&raquo;</label>";
            }

        }

        $sHtml =
            "<div class=\"tx-hive-cpt-cnt-cssslider\">" .
            "<div class='csss--vh iXs--$aClasses[0] iSm--$aClasses[1] iMd--$aClasses[2] iLg--$aClasses[3] iXl--$aClasses[4] iCount--$iCount' id='csss--$sUniqueIdentifier'>" .
            $sInputs .
            "<nav class='dots btn-group'>" .
            "<div class='prev btn btn-secondary'>" . $sArrowsPrev . "</div>" . $sDots . "<div class='next btn btn-secondary'>" . $sArrowsNext . "</div>" .
            "</nav>" .
            "<div class=\"csss__wrapper\">" .
            "<div class=\"csss__slider row d-flex iXs--$aClasses[0] iSm--$aClasses[1] iMd--$aClasses[2] iLg--$aClasses[3] iXl--$aClasses[4] iCount--$iCount\">" .
            "###SLIDES###" .
            "</div>" .
            "</div>" .
            "</div>" .
            "</div>";

        return $sHtml;

    }

    /**
     * @param int $iSlides
     * @param string $sUniqueIdentifier
     * @param array $aClasses
     * @param array $aNavLabelsOverrides
     *
     * @return string
     */
    public function createSliderFlavorJsHtml(
        int $iSlides = 0,
        string $sUniqueIdentifier = "",
        array $aClasses = [1, 1, 1, 1, 1],
        array $aNavLabelsOverrides = []
    ): string {

        if ($aClasses[0] < 1 or $aClasses[0] > 1) {
            return "<div class=\"alert alert-danger\" role=\"alert\">Error " . __CLASS__ . " " . __FUNCTION__ . " [" . __LINE__ . "]</div>";
        }
        if ($aClasses[1] < 1 or $aClasses[1] > 1) {
            return "<div class=\"alert alert-danger\" role=\"alert\">Error " . __CLASS__ . " " . __FUNCTION__ . " [" . __LINE__ . "]</div>";
        }
        if ($aClasses[2] < 1 or $aClasses[2] > 2) {
            return "<div class=\"alert alert-danger\" role=\"alert\">Error " . __CLASS__ . " " . __FUNCTION__ . " [" . __LINE__ . "]</div>";
        }
        if ($aClasses[3] < 1 or $aClasses[3] > 3) {
            return "<div class=\"alert alert-danger\" role=\"alert\">Error " . __CLASS__ . " " . __FUNCTION__ . " [" . __LINE__ . "]</div>";
        }
        if ($aClasses[4] < 1 or $aClasses[4] > 5) {
            return "<div class=\"alert alert-danger\" role=\"alert\">Error " . __CLASS__ . " " . __FUNCTION__ . " [" . __LINE__ . "]</div>";
        }

        $iCount = $iSlides;

        if ($iCount < 1) {
            return "<div class=\"alert alert-danger\" role=\"alert\">Error " . __CLASS__ . " " . __FUNCTION__ . " [" . __LINE__ . "]</div>";
        }

        foreach ($aClasses as $iK => $iV) {
            if ($iCount < $iV) {
                $aClasses[$iK] = $iCount;
            }
        }

        $sInputs = "";
        $sDots = "";

        $sArrowsPrev = "";

        $sArrowsNext = "";
        $sSlides = "";
        for ($i = 1; $i <= $iCount; $i++) {
            $sNavLabel = (array_key_exists($i-1, $aNavLabelsOverrides) && $aNavLabelsOverrides[$i-1] != "" ? $aNavLabelsOverrides[$i-1] : $i);
            $sDots .= "<a href='javascript:;' class='dot dot--csssjs--$sUniqueIdentifier btn btn-secondary' data-to='$i'>$sNavLabel</a>";
        }

        $sArrowsPrev .= "<a href='javascript:;' class='prev prev--csssjs btn btn-secondary' id='prev--csssjs--$sUniqueIdentifier' data-slider-id='csssjs--$sUniqueIdentifier'>&laquo;</a>";
        $sArrowsNext .= "<a href='javascript:;' class='next next--csssjs btn btn-secondary' id='next--csssjs--$sUniqueIdentifier' data-slider-id='csssjs--$sUniqueIdentifier'>&raquo;</a>";

        /*
         * Create Css
         */
        $iWidthXs = $iCount * (100/$aClasses[0]);
        $iWidthSm = $iCount * (100/$aClasses[1]);
        $iWidthMd = $iCount * (100/$aClasses[2]);
        $iWidthLg = $iCount * (100/$aClasses[3]);
        $iWidthXl = $iCount * (100/$aClasses[4]);
        $sCss = "<style>";
        $sCss .= "#csssjs--vh--$sUniqueIdentifier .csssjs__wrapper .csssjs__slider{width:$iWidthXs%;}";
        $sCss .= "@media (min-width: 576px){#csssjs--vh--$sUniqueIdentifier .csssjs__wrapper .csssjs__slider{width:$iWidthSm%;}}";
        $sCss .= "@media (min-width: 768px){#csssjs--vh--$sUniqueIdentifier .csssjs__wrapper .csssjs__slider{width:$iWidthMd%;}}";
        $sCss .= "@media (min-width: 992px){#csssjs--vh--$sUniqueIdentifier .csssjs__wrapper .csssjs__slider{width:$iWidthLg%;}}";
        $sCss .= "@media (min-width: 1200px){#csssjs--vh--$sUniqueIdentifier .csssjs__wrapper .csssjs__slider{width:$iWidthXl%;}}";
        $sCss .= "</style>";

        /*
         * Create Js
         *
         * Js placed under slider element => slider controls work instant.
         * TODO _Maybe_ move Js to concatenated Js files into footer. But then controls do not work until whole HTML is loaded.
         */
        $sJs = "<script type='text/Javascript'>";
        $sJs .= "
var oSliderJsFlavor=document.getElementsByClassName(\"csssjs--vh--$sUniqueIdentifier\");if(null!=oSliderJsFlavor&&1==oSliderJsFlavor.length){var sSliderJsFlavorId=oSliderJsFlavor[0].getAttribute(\"id\");hivecptcntcssslider__oSliderJsFlavor[sSliderJsFlavorId]=new Object,hivecptcntcssslider__oSliderJsFlavor[sSliderJsFlavorId].iCount=oSliderJsFlavor[0].getAttribute(\"data-count\"),hivecptcntcssslider__oSliderJsFlavor[sSliderJsFlavorId].iActive=1;var oSliderJsFlavorPrev=document.getElementById(\"prev--csssjs--$sUniqueIdentifier\");if(null!=oSliderJsFlavorPrev&&\"undefined\"!=typeof oSliderJsFlavorPrev)oSliderJsFlavorPrev.addEventListener(\"click\",function(){var e=hivecptcntcssslider__oSliderJsFlavor[\"csssjs--vh--$sUniqueIdentifier\"].iActive,t=hivecptcntcssslider__oSliderJsFlavor[\"csssjs--vh--$sUniqueIdentifier\"].iCount,s=1==e?1:e-1,i=document.getElementById(\"csssjs__wrapper--$sUniqueIdentifier\"),r=document.getElementById(\"csssjs__slider--$sUniqueIdentifier\");if(null!=i&&\"undefined\"!=typeof i&&null!=r&&\"undefined\"!=typeof r){var n=s*(100/t)*-1+100/t;if(r.setAttribute(\"style\",\"transform:translate3d(\"+n+\"%,0,0);\"),hivecptcntcssslider__oSliderJsFlavor[\"csssjs--vh--$sUniqueIdentifier\"].iActive=s,\"object\"==typeof bLazy)setTimeout(function(){bLazy.revalidate()},1500)}});var oSliderJsFlavorNext=document.getElementById(\"next--csssjs--$sUniqueIdentifier\");if(null!=oSliderJsFlavorNext&&\"undefined\"!=typeof oSliderJsFlavorNext)oSliderJsFlavorNext.addEventListener(\"click\",function(){var e=document.getElementById(\"csssjs__wrapper--$sUniqueIdentifier\").getBoundingClientRect(),t=document.getElementById(\"csssjs__slider--$sUniqueIdentifier\").getBoundingClientRect(),s={};s.right=t.right-e.right;var i=hivecptcntcssslider__oSliderJsFlavor[\"csssjs--vh--$sUniqueIdentifier\"].iActive,r=hivecptcntcssslider__oSliderJsFlavor[\"csssjs--vh--$sUniqueIdentifier\"].iCount,n=i==r?1:0==Math.round(s.right)?1:i+1,d=document.getElementById(\"csssjs__wrapper--$sUniqueIdentifier\"),l=document.getElementById(\"csssjs__slider--$sUniqueIdentifier\");if(null!=d&&\"undefined\"!=typeof d&&null!=l&&\"undefined\"!=typeof l){var o=n*(100/r)*-1+100/r;if(l.setAttribute(\"style\",\"transform:translate3d(\"+o+\"%,0,0);\"),hivecptcntcssslider__oSliderJsFlavor[\"csssjs--vh--$sUniqueIdentifier\"].iActive=n,\"object\"==typeof bLazy)setTimeout(function(){bLazy.revalidate()},1500)}});var oSliderJsFlavorDot=document.getElementsByClassName(\"dot--csssjs--$sUniqueIdentifier\");if(0!=oSliderJsFlavorDot.length)for(var d=0;d<oSliderJsFlavorDot.length;d++)oSliderJsFlavorDot[d].addEventListener(\"click\",function(){var e=document.getElementById(\"csssjs__wrapper--$sUniqueIdentifier\").getBoundingClientRect(),t=document.getElementById(\"csssjs__slider--$sUniqueIdentifier\").getBoundingClientRect(),s={};s.right=t.right-e.right;var i=hivecptcntcssslider__oSliderJsFlavor[\"csssjs--vh--$sUniqueIdentifier\"].iActive,r=hivecptcntcssslider__oSliderJsFlavor[\"csssjs--vh--$sUniqueIdentifier\"].iCount,n=parseInt(this.getAttribute(\"data-to\"));if(0==Math.round(s.right)&&n>i)n=i;var d=document.getElementsByClassName(\"csssjs--vh--$sUniqueIdentifier\");switch(hivecptcntcssslider__getCurrentMediaQuery()){case\"xs\":l=parseInt(d[0].getAttribute(\"data-show-xs\"));break;case\"sm\":l=parseInt(d[0].getAttribute(\"data-show-sm\"));break;case\"md\":l=parseInt(d[0].getAttribute(\"data-show-md\"));break;case\"lg\":l=parseInt(d[0].getAttribute(\"data-show-lg\"));break;case\"xl\":var l=parseInt(d[0].getAttribute(\"data-show-xl\"));break;default:l=1}if(n>r-(l-1))n=r-(l-1);var o=document.getElementById(\"csssjs__wrapper--$sUniqueIdentifier\"),a=document.getElementById(\"csssjs__slider--$sUniqueIdentifier\");if(null!=o&&\"undefined\"!=typeof o&&null!=a&&\"undefined\"!=typeof a){var c=n*(100/r)*-1+100/r;if(a.setAttribute(\"style\",\"transform:translate3d(\"+c+\"%,0,0);\"),hivecptcntcssslider__oSliderJsFlavor[\"csssjs--vh--$sUniqueIdentifier\"].iActive=n,\"object\"==typeof bLazy)setTimeout(function(){bLazy.revalidate()},1500)}})}
        ";
        $sJs .= "</script>";

        $sHtml =
            $sCss .
            "<div class=\"tx-hive-cpt-cnt-cssslider\">" .
            "<div class='csssjs--vh csssjs--vh--$sUniqueIdentifier'
                id='csssjs--vh--$sUniqueIdentifier'
                data-count='$iCount'
                data-show-xs='$aClasses[0]'
                data-show-sm='$aClasses[1]'
                data-show-md='$aClasses[2]'
                data-show-lg='$aClasses[3]'
                data-show-xl='$aClasses[4]'
                >" .
            $sInputs .
            "<nav class='btn-group'>" . $sArrowsPrev . "</nav>" .
            "<nav class='dots btn-group'>" . $sDots . "</nav>" .
            "<nav class='btn-group'>" . $sArrowsNext . "</nav>" .
            "<div class=\"csssjs__wrapper csssjs__wrapper--$sUniqueIdentifier\" id=\"csssjs__wrapper--$sUniqueIdentifier\">" .
            "<div class=\"csssjs__slider csssjs__slider--$sUniqueIdentifier row d-flex\" id=\"csssjs__slider--$sUniqueIdentifier\">" .
            "###SLIDES###" .
            "</div>" .
            "</div>" .
            "</div>" .
            "</div>" .
            $sJs;

        return $sHtml;

    }


}
