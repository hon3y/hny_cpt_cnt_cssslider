<?php

namespace HIVE\HiveCptCntCssslider\Hooks\PageLayoutView;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use \TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface;
use \TYPO3\CMS\Backend\View\PageLayoutView;
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use \HIVE\HiveUserfuncs\UserFunc\SettingsUserFunc;

/**
 * Contains a preview rendering for the page module of CType="yourextensionkey_newcontentelement"
 */
class CsssliderPreviewRenderer implements PageLayoutViewDrawItemHookInterface
{
    /**
     * Preprocesses the preview rendering of a content element of type "My new content element"
     *
     * @param \TYPO3\CMS\Backend\View\PageLayoutView $parentObject Calling parent object
     * @param bool $drawItem Whether to draw the item using the default functionality
     * @param string $headerContent Header content
     * @param string $itemContent Item content
     * @param array $row Record row of tt_content
     *
     * @return void
     */
    public function preProcess(
        PageLayoutView &$parentObject,
        &$drawItem,
        &$headerContent,
        &$itemContent,
        array &$row
    ) {

        if ($row['list_type'] !== 'hivecptcntcssslider_cssslideroneonetwothreefour'
            && $row['list_type'] !== 'hivecptcntcssslider_cssslideroneonetwothreefourjs'
            && $row['list_type'] !== 'hivecptcntcssslider_cssslideroneoneoneoneone') {
            return;
        } else {

            $sDynamicContentPart = '';
            $aFlexformXml = GeneralUtility::xml2array($row['pi_flexform']);
            if (is_array($aFlexformXml)) {
                if (array_key_exists("data", $aFlexformXml) and is_array($aFlexformXml["data"])) {
                    if (array_key_exists("sDEF",
                            $aFlexformXml["data"]) and is_array($aFlexformXml["data"]["sDEF"])) {
                        if (array_key_exists("lDEF",
                                $aFlexformXml["data"]["sDEF"]) and is_array($aFlexformXml["data"]["sDEF"]["lDEF"])) {
                            if (array_key_exists("settings.cssslider.parentPageUid",
                                    $aFlexformXml["data"]["sDEF"]["lDEF"])
                                and is_array($aFlexformXml["data"]["sDEF"]["lDEF"]["settings.cssslider.parentPageUid"])) {

                                if (array_key_exists("vDEF",
                                    $aFlexformXml["data"]["sDEF"]["lDEF"]["settings.cssslider.parentPageUid"])) {

                                    $iParentPageUid = intval($aFlexformXml["data"]["sDEF"]["lDEF"]["settings.cssslider.parentPageUid"]["vDEF"]);
                                    $aSlidePageUids = $this->getPageUidsForGivenParentPageUid($iParentPageUid);

                                    foreach ($aSlidePageUids as $iSlidePageUid) {
                                        $sDynamicContentPart .=
                                            '<li class="list-group-item">' .
                                            '[' .
                                            $iSlidePageUid .
                                            '] ' .
                                            $this->getPageTitleByUid(intval($iSlidePageUid)) .
                                            '</li>';
                                    }
                                }
                            }
                        }
                    }
                }

                $sCardHeader = '';
                if ($row['list_type'] == 'hivecptcntcssslider_cssslideroneonetwothreefour') {
                    $sCardHeader .= '
                        <kbd>1 [XS]</kbd>
                        <kbd>1 [SM]</kbd> 
                        <kbd>2 [MD]</kbd> 
                        <kbd>3 [LG]</kbd> 
                        <kbd>4 [XL]</kbd>';
                }
                if ($row['list_type'] == 'hivecptcntcssslider_cssslideroneonetwothreefourjs') {
                    $sCardHeader .= '
                        <kbd>1 [XS]</kbd>
                        <kbd>1 [SM]</kbd> 
                        <kbd>2 [MD]</kbd> 
                        <kbd>3 [LG]</kbd> 
                        <kbd>4 [XL]</kbd>';
                }
                if ($row['list_type'] == 'hivecptcntcssslider_cssslideroneoneoneoneone') {
                    $sCardHeader .= '
                        <kbd>1 [XS]</kbd>
                        <kbd>1 [SM]</kbd> 
                        <kbd>1 [MD]</kbd> 
                        <kbd>1 [LG]</kbd> 
                        <kbd>1 [XL]</kbd>';
                }

                // preview html
                $itemContent .= '            
                
                    <style>
                    .card.csss--backend {
                        background: #ededed;
                    }
                    .card.csss--backend .card-header,
                    .card.csss--backend .card-body {
                        padding: 10px;
                        border: solid 1px #ccc;
                        border-bottom: none;
                    }
                    </style>               
                    <div class="card csss--backend">
                        <div class="card-header">
                            <strong>Pure Css Slider</strong><br>
                            ' . $sCardHeader . '
                        </div>
                        <ul class="list-group list-group-flush">'
                                . $sDynamicContentPart .
                                '</ul>
                        </div>
                    </div>';

            }

            $headerContent .= '';
            $drawItem = false;
        }
    }

    protected function getPageUidsForGivenParentPageUid(int $iParentPageUid): array
    {
        // get data
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('pages');
        $queryBuilder
            ->select('pages.uid AS uid')
            ->from('pages')
            ->where('pages.pid IN (' . $iParentPageUid . ')')
            ->andWhere('pages.hidden = 0')
            ->andWhere('pages.deleted = 0');

        $aResult = $queryBuilder->execute()->fetchAll();
        $aReturn = [];
        foreach ($aResult as $aUid) {
            $aReturn[] = $aUid["uid"];
        }

        return $aReturn;
    }

    /**
     * @param int $iPageUid
     *
     * @return mixed
     */
    protected function getPageTitleByUid(int $iPageUid)
    {

        // get data
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('pages');
        $queryBuilder
            ->select('pages.title AS title')
            ->from('pages')
            ->where('pages.uid IN (' . $iPageUid . ')')
            ->andWhere('pages.hidden = 0')
            ->andWhere('pages.deleted = 0');

        $aResult = $queryBuilder->execute()->fetchAll();
        if (is_array($aResult) and count($aResult) > 0) {
            return $aResult[0]["title"];
        }

    }
}