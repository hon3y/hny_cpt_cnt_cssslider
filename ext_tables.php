<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveCptCntCssslider',
            'Cssslideroneonetwothreefour',
            'Css Slider :: One - Two - Three - Four'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveCptCntCssslider',
            'Cssslideroneoneoneoneone',
            'Css Slider :: One - One - One - One'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveCptCntCssslider',
            'Cssslideroneonetwothreefourjs',
            'Css Slider :: One - Two - Three - Four - Js'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_cpt_cnt_cssslider', 'Configuration/TypoScript', 'hive_cpt_cnt_cssslider');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hivecptcntcssslider_domain_model_render', 'EXT:hive_cpt_cnt_cssslider/Resources/Private/Language/locallang_csh_tx_hivecptcntcssslider_domain_model_render.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hivecptcntcssslider_domain_model_render');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder


call_user_func_array(
    function ($extkey, &$tca) {
        $extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($extkey));
        $pluginName = strtolower('Cssslideroneonetwothreefour');
        $pluginSignature = $extensionName . '_' . $pluginName;
        $tca['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
        $tca['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature,
            'FILE:EXT:' . $extkey . '/Configuration/FlexForms/Cssslideroneonetwothreefour.xml');

        $extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($extkey));
        $pluginName = strtolower('Cssslideroneonetwothreefourjs');
        $pluginSignature = $extensionName . '_' . $pluginName;
        $tca['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
        $tca['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature,
            'FILE:EXT:' . $extkey . '/Configuration/FlexForms/Cssslideroneonetwothreefourjs.xml');

        $extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($extkey));
        $pluginName = strtolower('Cssslideroneoneoneoneone');
        $pluginSignature = $extensionName . '_' . $pluginName;
        $tca['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
        $tca['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature,
            'FILE:EXT:' . $extkey . '/Configuration/FlexForms/Cssslideroneoneoneoneone.xml');


    }, [$_EXTKEY, &$TCA]
);