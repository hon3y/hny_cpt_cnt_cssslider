
plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefour {
    view {
        templateRootPaths.0 = EXT:hive_cpt_cnt_cssslider/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefour.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_cpt_cnt_cssslider/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefour.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_cpt_cnt_cssslider/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefour.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefour.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

plugin.tx_hivecptcntcssslider_cssslideroneoneoneoneone {
    view {
        templateRootPaths.0 = EXT:hive_cpt_cnt_cssslider/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hivecptcntcssslider_cssslideroneoneoneoneone.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_cpt_cnt_cssslider/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hivecptcntcssslider_cssslideroneoneoneoneone.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_cpt_cnt_cssslider/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hivecptcntcssslider_cssslideroneoneoneoneone.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hivecptcntcssslider_cssslideroneoneoneoneone.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefourjs {
    view {
        templateRootPaths.0 = EXT:hive_cpt_cnt_cssslider/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefourjs.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_cpt_cnt_cssslider/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefourjs.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_cpt_cnt_cssslider/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefourjs.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefourjs.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_hivecptcntcssslider._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-hive-cpt-cnt-cssslider table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-hive-cpt-cnt-cssslider table th {
        font-weight:bold;
    }

    .tx-hive-cpt-cnt-cssslider table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

lib.site.anchorslide = COA
lib.site.anchorslide < lib.site.anchor
lib.site.anchorslide.10.select.max = 1
lib.site.anchorslide.10.renderObj.15.1000.select.max = 1

lib.site.anchorslide.10.renderObj.15.stdWrap.dataWrap = <a id="page-{field:uid}" name="page-{field:uid}"></a><section id="section__uid--{field:uid}" data-section="{field:uid}" class="{register:sectionClasses100} d-flex section__bl--{field:backend_layout} section__doktype--{field:doktype} {field:tx_hivecptanchornav_bs4_class_section}" data-sys_language_uid="{TSFE:sys_language_uid}">|</section>

lib.site.anchorslide.10.renderObj.15.1000.renderObj.20.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:rowDivClasses100} layout-{field:layout}">|</div>
lib.site.anchorslide.10.renderObj.15.1000.renderObj.30.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:rowDivClasses5050} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div>
lib.site.anchorslide.10.renderObj.15.1000.renderObj.40.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:rowDivClasses3366} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div>
lib.site.anchorslide.10.renderObj.15.1000.renderObj.50.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:rowDivClasses6633} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div>
lib.site.anchorslide.10.renderObj.15.1000.renderObj.80.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:rowDivClasses2575} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div>
lib.site.anchorslide.10.renderObj.15.1000.renderObj.90.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:rowDivClasses7525} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div>
lib.site.anchorslide.10.renderObj.15.1000.renderObj.100.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:rowDivClasses4060} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div>
lib.site.anchorslide.10.renderObj.15.1000.renderObj.110.stdWrap.dataWrap = <a id="row-{field:uid}" name="row-{field:uid}"></a><div class="{register:rowDivClasses6040} layout-{field:layout} {field:tx_hivecptanchornav_bs4_align_row} {field:tx_hivecptanchornav_bs4_no_gutters_row}">|</div>

lib.site.anchorslide.10.renderObj.15.1000.renderObj.25 >
lib.site.anchorslide.10.renderObj.15.1000.renderObj.35 >
lib.site.anchorslide.10.renderObj.15.1000.renderObj.45 >
lib.site.anchorslide.10.renderObj.15.1000.renderObj.55 >
lib.site.anchorslide.10.renderObj.15.1000.renderObj.60 >
lib.site.anchorslide.10.renderObj.15.1000.renderObj.65 >
lib.site.anchorslide.10.renderObj.15.1000.renderObj.70 >
lib.site.anchorslide.10.renderObj.15.1000.renderObj.75 >
lib.site.anchorslide.10.renderObj.15.1000.renderObj.85 >
lib.site.anchorslide.10.renderObj.15.1000.renderObj.95 >
lib.site.anchorslide.10.renderObj.15.1000.renderObj.105 >
lib.site.anchorslide.10.renderObj.15.1000.renderObj.115 >
lib.site.anchorslide.10.renderObj.15.1000.renderObj.120 >
lib.site.anchorslide.10.renderObj.15.1000.renderObj.125 >