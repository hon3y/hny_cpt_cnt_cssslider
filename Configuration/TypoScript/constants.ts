
plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefour {
    view {
        # cat=plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefour/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_cpt_cnt_cssslider/Resources/Private/Templates/
        # cat=plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefour/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_cpt_cnt_cssslider/Resources/Private/Partials/
        # cat=plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefour/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_cpt_cnt_cssslider/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefour//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_hivecptcntcssslider_cssslideroneoneoneoneone {
    view {
        # cat=plugin.tx_hivecptcntcssslider_cssslideroneoneoneoneone/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_cpt_cnt_cssslider/Resources/Private/Templates/
        # cat=plugin.tx_hivecptcntcssslider_cssslideroneoneoneoneone/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_cpt_cnt_cssslider/Resources/Private/Partials/
        # cat=plugin.tx_hivecptcntcssslider_cssslideroneoneoneoneone/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_cpt_cnt_cssslider/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hivecptcntcssslider_cssslideroneoneoneoneone//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefourjs {
    view {
        # cat=plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefourjs/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_cpt_cnt_cssslider/Resources/Private/Templates/
        # cat=plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefourjs/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_cpt_cnt_cssslider/Resources/Private/Partials/
        # cat=plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefourjs/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_cpt_cnt_cssslider/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hivecptcntcssslider_cssslideroneonetwothreefourjs//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder