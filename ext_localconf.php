<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCptCntCssslider',
            'Cssslideroneonetwothreefour',
            [
                'Render' => 'oneOneTwoThreeFour'
            ],
            // non-cacheable actions
            [
                'Render' => ''
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCptCntCssslider',
            'Cssslideroneoneoneoneone',
            [
                'Render' => 'oneOneOneOneOne'
            ],
            // non-cacheable actions
            [
                'Render' => ''
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCptCntCssslider',
            'Cssslideroneonetwothreefourjs',
            [
                'Render' => 'oneOneTwoThreeFourJs'
            ],
            // non-cacheable actions
            [
                'Render' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    cssslideroneonetwothreefour {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_cpt_cnt_cssslider') . 'Resources/Public/Icons/user_plugin_cssslideroneonetwothreefour.svg
                        title = LLL:EXT:hive_cpt_cnt_cssslider/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_cssslider_domain_model_cssslideroneonetwothreefour
                        description = LLL:EXT:hive_cpt_cnt_cssslider/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_cssslider_domain_model_cssslideroneonetwothreefour.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntcssslider_cssslideroneonetwothreefour
                        }
                    }
                    cssslideroneoneoneoneone {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_cpt_cnt_cssslider') . 'Resources/Public/Icons/user_plugin_cssslideroneoneoneoneone.svg
                        title = LLL:EXT:hive_cpt_cnt_cssslider/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_cssslider_domain_model_cssslideroneoneoneoneone
                        description = LLL:EXT:hive_cpt_cnt_cssslider/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_cssslider_domain_model_cssslideroneoneoneoneone.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntcssslider_cssslideroneoneoneoneone
                        }
                    }
                    cssslideroneonetwothreefourjs {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_cpt_cnt_cssslider') . 'Resources/Public/Icons/user_plugin_cssslideroneonetwothreefourjs.svg
                        title = LLL:EXT:hive_cpt_cnt_cssslider/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_cssslider_domain_model_cssslideroneonetwothreefourjs
                        description = LLL:EXT:hive_cpt_cnt_cssslider/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_cssslider_domain_model_cssslideroneonetwothreefourjs.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntcssslider_cssslideroneonetwothreefourjs
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

call_user_func(
    function ($extKey, $globals) {

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.cssslideroneonetwothreefour >
                wizards.newContentElement.wizardItems.plugins.elements.cssslideroneonetwothreefourjs >
                wizards.newContentElement.wizardItems.plugins.elements.cssslideroneoneoneoneone >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.cssslideroneonetwothreefour {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = (Pure) Css Multi-Slider
                            description = 100% [XS,SM] | 50% 50% [MD] | 33% 33% 33% [LG] | 25% 25% 25% 25% [XL]
                            tt_content_defValues {
                                CType = list
                                list_type = hivecptcntcssslider_cssslideroneonetwothreefour
                            }
                        }
                        elements.cssslideroneonetwothreefourjs {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = Css Multi-Slider - JS flavor
                            description = 100% [XS,SM] | 50% 50% [MD] | 33% 33% 33% [LG] | 25% 25% 25% 25% [XL]
                            tt_content_defValues {
                                CType = list
                                list_type = hivecptcntcssslider_cssslideroneonetwothreefourjs
                            }
                        }
                        elements.cssslideroneoneoneoneone {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = (Pure) Css Slider
                            description = 100% [XS,SM,MD,LG,XL]
                            tt_content_defValues {
                                CType = list
                                list_type = hivecptcntcssslider_cssslideroneoneoneoneone
                            }
                        }
                        show := addToList(cssslideroneonetwothreefour,cssslideroneonetwothreefourjs,cssslideroneoneoneoneone)
                    }

                }
            }'
        );

        // Register for hook to show preview of tt_content element of CType="yourextensionkey_newcontentelement" in page module
        $globals['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem'][$extKey] =
            \HIVE\HiveCptCntCssslider\Hooks\PageLayoutView\CsssliderPreviewRenderer::class;

    }, $_EXTKEY, $GLOBALS
);