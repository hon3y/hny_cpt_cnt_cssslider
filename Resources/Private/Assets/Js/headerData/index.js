var hivecptcntcssslider__oSliderJsFlavor = new Object();
var hivecptcntcssslider__oSliderJsFlavorPrev = new Object();
var hivecptcntcssslider__oSliderJsFlavorNext = new Object();

function hivecptcntcssslider__getCurrentMediaQuery() {
    var oWindow = window
        , sInner = "inner";
    if (!("innerWidth"in window)) {
        sInner = "client";
        oWindow = document.documentElement || document.body
    }
    var iW = oWindow[sInner + "Width"];
    return ( iW < 576 ? "xs" : ( iW < 768 ? "sm" : ( iW < 992 ? "md" : ( iW < 1200 ? "lg" : "xl" ) ) ) );
}