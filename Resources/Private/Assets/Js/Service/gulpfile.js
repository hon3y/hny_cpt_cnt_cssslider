/**
 * Includes
 */
var gulp = require('gulp');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');


var paths = {
    src: {
        js: [
            'In/*.js'
        ]
    },
    dest: {
        js: 'Out/'
    }
};

/**
 * Js => Js.min
 */
gulp.task('js', function() {
    return gulp.src(
        paths.src.js
        )
        .pipe(concat('index.js'))
        .pipe(gulp.dest(paths.dest.js))
        .pipe(rename('index.min.js'))
        .pipe(uglify({compress: {
                comparisons: false,
                conditionals: false
            }}))
        .pipe(gulp.dest(paths.dest.js));
});

/**
 * Fly Willy fly ;)
 */
gulp.task(
    'default',
    [
        'js'
    ],
    function() {
        gulp.watch(paths.src.js, ['js'])
            .on('change', function(evt) {
                console.log(
                    '[watcher] File ' + evt.path + ' was ' + evt.type + ', compiling...'
                );
            });
    }
);