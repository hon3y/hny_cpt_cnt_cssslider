/**
 * Js Flavor
 */
var oSliderJsFlavor = document.getElementsByClassName("csssjs--vh--$sUniqueIdentifier");
if (oSliderJsFlavor != null && oSliderJsFlavor.length == 1) {
    var sSliderJsFlavorId = oSliderJsFlavor[0].getAttribute("id");
    hivecptcntcssslider__oSliderJsFlavor[sSliderJsFlavorId] = new Object();
    hivecptcntcssslider__oSliderJsFlavor[sSliderJsFlavorId]["iCount"] = oSliderJsFlavor[0].getAttribute("data-count");
    hivecptcntcssslider__oSliderJsFlavor[sSliderJsFlavorId]["iActive"] = 1;

    var oSliderJsFlavorPrev = document.getElementById("prev--csssjs--$sUniqueIdentifier");
    if (oSliderJsFlavorPrev != null && typeof oSliderJsFlavorPrev != 'undefined') {
        oSliderJsFlavorPrev.addEventListener("click", function() {
            var iActive = hivecptcntcssslider__oSliderJsFlavor["csssjs--vh--$sUniqueIdentifier"]["iActive"];
            var iCount = hivecptcntcssslider__oSliderJsFlavor["csssjs--vh--$sUniqueIdentifier"]["iCount"];
            var iPrev = (iActive == 1 ? 1 : (iActive-1));
            var oSliderJsFlavorWrapper = document.getElementById("csssjs__wrapper--$sUniqueIdentifier");
            var oSliderJsFlavorSlider = document.getElementById("csssjs__slider--$sUniqueIdentifier");
            if (oSliderJsFlavorWrapper != null && typeof oSliderJsFlavorWrapper != 'undefined'
                && oSliderJsFlavorSlider != null && typeof oSliderJsFlavorSlider != 'undefined') {
                var oSliderJsFlavorTranslate = (-1 * (iPrev * (100/iCount))) + (100/iCount);
                oSliderJsFlavorSlider.setAttribute("style",'transform:translate3d(' + oSliderJsFlavorTranslate + '%,0,0);');
                hivecptcntcssslider__oSliderJsFlavor["csssjs--vh--$sUniqueIdentifier"]["iActive"] = iPrev;

                if (typeof bLazy == 'object') {
                    setTimeout(function() {
                        bLazy.revalidate();
                    }, 1500);
                }

            }
        });
    }

    var oSliderJsFlavorNext = document.getElementById("next--csssjs--$sUniqueIdentifier");
    if (oSliderJsFlavorNext != null && typeof oSliderJsFlavorNext != 'undefined') {
        oSliderJsFlavorNext.addEventListener("click", function() {
            var parentPos = document.getElementById('csssjs__wrapper--$sUniqueIdentifier').getBoundingClientRect(),
                childrenPos = document.getElementById('csssjs__slider--$sUniqueIdentifier').getBoundingClientRect(),
                relativePos = {};
            relativePos.right = childrenPos.right - parentPos.right;
            var iActive = hivecptcntcssslider__oSliderJsFlavor["csssjs--vh--$sUniqueIdentifier"]["iActive"];
            var iCount = hivecptcntcssslider__oSliderJsFlavor["csssjs--vh--$sUniqueIdentifier"]["iCount"];
            var iNext = (iActive == iCount ? 1 : (Math.round(relativePos.right) == 0 ? 1 : (iActive+1)));
            var oSliderJsFlavorWrapper = document.getElementById("csssjs__wrapper--$sUniqueIdentifier");
            var oSliderJsFlavorSlider = document.getElementById("csssjs__slider--$sUniqueIdentifier");
            if (oSliderJsFlavorWrapper != null && typeof oSliderJsFlavorWrapper != 'undefined'
                && oSliderJsFlavorSlider != null && typeof oSliderJsFlavorSlider != 'undefined') {
                var oSliderJsFlavorTranslate = (-1 * (iNext * (100/iCount))) + (100/iCount);
                oSliderJsFlavorSlider.setAttribute("style",'transform:translate3d(' + oSliderJsFlavorTranslate + '%,0,0);');
                hivecptcntcssslider__oSliderJsFlavor["csssjs--vh--$sUniqueIdentifier"]["iActive"] = iNext;

                if (typeof bLazy == 'object') {
                    setTimeout(function() {
                        bLazy.revalidate();
                    }, 1500);
                }
            }
        });
    }

    var oSliderJsFlavorDot = document.getElementsByClassName("dot--csssjs--$sUniqueIdentifier");
    if (oSliderJsFlavorDot.length != 0) {
        for (var d = 0; d < oSliderJsFlavorDot.length; d++) {
            oSliderJsFlavorDot[d].addEventListener("click", function() {
                var parentPos = document.getElementById('csssjs__wrapper--$sUniqueIdentifier').getBoundingClientRect(),
                    childrenPos = document.getElementById('csssjs__slider--$sUniqueIdentifier').getBoundingClientRect(),
                    relativePos = {};
                relativePos.right = childrenPos.right - parentPos.right;
                var iActive = hivecptcntcssslider__oSliderJsFlavor["csssjs--vh--$sUniqueIdentifier"]["iActive"];
                var iCount = hivecptcntcssslider__oSliderJsFlavor["csssjs--vh--$sUniqueIdentifier"]["iCount"];
                var iDot = parseInt(this.getAttribute("data-to"));
                if ((Math.round(relativePos.right) == 0) && iDot > iActive) {
                    iDot = iActive;
                }

                var oSliderJsFlavor_ = document.getElementsByClassName("csssjs--vh--$sUniqueIdentifier");
                var sCMQ = hivecptcntcssslider__getCurrentMediaQuery();
                switch (sCMQ) {
                    case "xs":
                        var iShow = parseInt(oSliderJsFlavor_[0].getAttribute("data-show-xs"));
                        break;
                    case "sm":
                        var iShow = parseInt(oSliderJsFlavor_[0].getAttribute("data-show-sm"));
                        break;
                    case "md":
                        var iShow = parseInt(oSliderJsFlavor_[0].getAttribute("data-show-md"));
                        break;
                    case "lg":
                        var iShow = parseInt(oSliderJsFlavor_[0].getAttribute("data-show-lg"));
                        break;
                    case "xl":
                        var iShow = parseInt(oSliderJsFlavor_[0].getAttribute("data-show-xl"));
                        break;
                    default:
                        iShow = 1;
                }
                if (iDot > (iCount - (iShow-1))) {
                    iDot = (iCount - (iShow-1));
                }

                var oSliderJsFlavorWrapper = document.getElementById("csssjs__wrapper--$sUniqueIdentifier");
                var oSliderJsFlavorSlider = document.getElementById("csssjs__slider--$sUniqueIdentifier");
                if (oSliderJsFlavorWrapper != null && typeof oSliderJsFlavorWrapper != 'undefined'
                    && oSliderJsFlavorSlider != null && typeof oSliderJsFlavorSlider != 'undefined') {
                    var oSliderJsFlavorTranslate = (-1 * (iDot * (100/iCount))) + (100/iCount);
                    oSliderJsFlavorSlider.setAttribute("style",'transform:translate3d(' + oSliderJsFlavorTranslate + '%,0,0);');
                    hivecptcntcssslider__oSliderJsFlavor["csssjs--vh--$sUniqueIdentifier"]["iActive"] = iDot;

                    if (typeof bLazy == 'object') {
                        setTimeout(function() {
                            bLazy.revalidate();
                        }, 1500);
                    }
                }
            });
        }
    }

}



// var parentPos = document.getElementById('csssjs--vh--54').getBoundingClientRect(),
//     childrenPos = document.getElementById('csssjs__wrapper--54').getBoundingClientRect(),
//     relativePos = {};
//     relativePos.top = childrenPos.top - parentPos.top,
//     relativePos.right = childrenPos.right - parentPos.right,
//     relativePos.bottom = childrenPos.bottom - parentPos.bottom,
//     relativePos.left = childrenPos.left - parentPos.left;
//
// console.log(relativePos);