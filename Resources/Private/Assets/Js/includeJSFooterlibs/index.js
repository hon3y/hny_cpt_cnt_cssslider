var hivecptcntcssslider__interval = setInterval(function () {
    if (typeof hive_cfg_typoscript__windowLoad == 'undefined') {
    } else {
        if (typeof hive_cfg_typoscript__windowLoad == "boolean" && hive_cfg_typoscript__windowLoad) {
            if (typeof bLazy == 'undefined') {
            } else {
                if (typeof bLazy == 'object') {
                    clearInterval(hivecptcntcssslider__interval);
                    if (hive_cfg_typoscript_sStage == "prototype" || hive_cfg_typoscript_sStage == "development") {
                        console.info('Cssslider :: initialize');
                    }
                    var aCsssInputs = document.getElementsByClassName("csss_input");
                    if (aCsssInputs.length > 0) {
                        for (var i = 0; i < aCsssInputs.length; i++) {
                            aCsssInputs[i].addEventListener("change", function() {
                                setTimeout(function() {
                                    bLazy.revalidate();
                                }, 1500);
                            });
                        }
                    }
                    
                }
            }
        }
    }
}, 250);